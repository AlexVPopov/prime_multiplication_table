# frozen_string_literal: true

RSpec.describe 'Rubocop' do
  subject { `rubocop -c .rubocop.yml` }

  it { is_expected.to match(/no offenses detected/) }
end
