# frozen_string_literal: true

RSpec.describe 'print_table.rb' do
  context 'when invoked with --help' do
    subject { `ruby lib/print_table.rb --help` }

    let(:expected_output) do
      <<~OUTPUT
        Usage:
            ruby print_table.rb [options]

        Displays a multiplication table of prime numbers

        Options:
            -c, --count [INTEGER]            The number of primes to be included in the table. Optional. Default: 10

        Example:
            ruby print_table.rb -c 5
      OUTPUT
    end

    it 'prints a help message' do
      is_expected.to eq expected_output
    end
  end

  context 'when invoked with no arguments' do
    subject { `ruby lib/print_table.rb` }

    let(:expected_output) do
      <<~OUTPUT
           |   2   3   5   7  11  13  17  19  23  29
        ---+----------------------------------------
         2 |   4   6  10  14  22  26  34  38  46  58
         3 |   6   9  15  21  33  39  51  57  69  87
         5 |  10  15  25  35  55  65  85  95 115 145
         7 |  14  21  35  49  77  91 119 133 161 203
        11 |  22  33  55  77 121 143 187 209 253 319
        13 |  26  39  65  91 143 169 221 247 299 377
        17 |  34  51  85 119 187 221 289 323 391 493
        19 |  38  57  95 133 209 247 323 361 437 551
        23 |  46  69 115 161 253 299 391 437 529 667
        29 |  58  87 145 203 319 377 493 551 667 841
      OUTPUT
    end

    it 'prints a table for 10 primes' do
      is_expected.to eq expected_output
    end
  end

  context 'when invoked with a valid argument for count' do
    subject { `ruby lib/print_table.rb -c 3` }

    let(:expected_output) do
      <<~OUTPUT
          |  2  3  5
        --+---------
        2 |  4  6 10
        3 |  6  9 15
        5 | 10 15 25
      OUTPUT
    end

    it 'prints a table for the specified number of primes' do
      is_expected.to eq expected_output
    end
  end

  context 'when invoked with an invalid argument for count' do
    subject { `ruby lib/print_table.rb -c "six"` }

    it 'prints an error message' do
      is_expected.to match(/count is invalid/)
    end
  end
end
