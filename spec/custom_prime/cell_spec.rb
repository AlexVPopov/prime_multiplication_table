# frozen_string_literal: true

RSpec.describe CustomPrime::Cell do
  describe '#to_s' do
    context 'with no content, width or padding specified' do
      subject { described_class.new.to_s }

      it 'returns an empty string' do
        is_expected.to eq ''
      end
    end

    context 'with no content and width, but padding specified' do
      subject { described_class.new(padding: '@').to_s }

      it 'returns an empty string' do
        is_expected.to eq ''
      end
    end

    context 'with content, width and padding specified' do
      subject { described_class.new(content: 23, width: 5, padding: '@').to_s }

      it 'returns a correctly padded string' do
        is_expected.to eq '@@@23'
      end
    end

    context 'with width less than the width of the content' do
      subject { described_class.new(content: 1234, width: -99, padding: '@').to_s }

      it 'returns the full content without padding' do
        is_expected.to eq '1234'
      end
    end

    context 'when the specified content is not a string' do
      subject { described_class.new(content: [], width: 3).to_s }

      it 'returns the string representation of the content' do
        is_expected.to eq ' []'
      end
    end

    context 'when specifying width only' do
      subject { described_class.new(width: 2).to_s }

      it 'returns a blank string with the specified width' do
        is_expected.to eq '  '
      end
    end
  end
end
