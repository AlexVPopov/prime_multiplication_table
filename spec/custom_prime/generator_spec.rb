# frozen_string_literal: true

require 'prime'

RSpec.describe CustomPrime::Generator do
  describe '.first' do
    it 'raises an error for non-integer input' do
      expect { described_class.first('ten thousand') }.to raise_error ArgumentError
    end

    it 'raises an error for non-positive input' do
      expect { described_class.first(0) }.to raise_error ArgumentError
    end

    it 'returns correct result for positive input' do
      expect(described_class.first(10_000)).to eq Prime.first(10_000)
    end
  end

  describe '.prime?' do
    it 'raises an error for non-integer input' do
      expect { described_class.prime?('thirteen') }.to raise_error ArgumentError
    end

    it 'returns false for numbers less than 2' do
      expect(described_class.prime?(-1)).to eq false
      expect(described_class.prime?(0)).to eq false
      expect(described_class.prime?(1)).to eq false
    end

    it 'returns true for prime numbers' do
      expect(described_class.prime?(7)).to eq true
      expect(described_class.prime?(982_451_653)).to eq true
    end

    it 'returns false for non-prime numbers' do
      expect(described_class.prime?(4)).to eq false
      expect(described_class.prime?(982_451_654)).to eq false
    end
  end
end
