# frozen_string_literal: true

require_relative 'cell'
require_relative 'generator'

module CustomPrime
  class Table
    DASH = '-'
    private_constant :DASH

    PIPE = '|'
    private_constant :PIPE

    PLUS = '+'
    private_constant :PLUS

    def initialize(count, prime_generator = Generator)
      @primes = prime_generator.first(count)
    end

    def to_s
      [header, divider, body].join("\n")
    end

    private

    attr_reader :primes

    def header
      first_cell  = Cell.new(width: first_cell_width)
      second_cell = Cell.new(width: second_cell_width, content: PIPE)
      final_cells = primes.map do |prime|
        Cell.new(width: final_cell_width, content: prime)
      end

      [first_cell, second_cell, *final_cells].join
    end

    def divider
      first_cell  = Cell.new(width: first_cell_width, padding: DASH)
      second_cell = Cell.new(width: second_cell_width, content: PLUS, padding: DASH)
      final_cells = primes.map do |_prime|
        Cell.new(width: final_cell_width, padding: DASH)
      end

      [first_cell, second_cell, *final_cells].join
    end

    def body
      primes.map { |prime| body_row(prime) }.join("\n")
    end

    def body_row(first_prime)
      first_cell  = Cell.new(width: first_cell_width, content: first_prime)
      second_cell = Cell.new(width: second_cell_width, content: PIPE)
      final_cells = primes.map do |prime|
        Cell.new(width: final_cell_width, content: prime * first_prime)
      end

      [first_cell, second_cell, *final_cells].join
    end

    def first_cell_width
      @first_cell_width ||= primes.last.to_s.size
    end

    def second_cell_width
      2
    end

    def final_cell_width
      @final_cell_width ||= (primes.last**2).to_s.size + 1
    end
  end
end
