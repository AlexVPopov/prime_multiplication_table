# frozen_string_literal: true

module CustomPrime
  module Generator
    module_function

    def first(index)
      raise ArgumentError, 'index must be an integer' unless index.is_a?(Integer)
      raise ArgumentError, 'index must be greater than 0' if index < 1

      primes = [2]

      return primes if index == 1

      3.step(by: 2).each do |current_number|
        next unless prime?(current_number)
        primes << current_number
        return primes if primes.length == index
      end
    end

    def prime?(number)
      raise ArgumentError, 'number must be an integer' unless number.is_a?(Integer)

      return false if number < 2

      2.upto(Math.sqrt(number)).none? { |current_number| (number % current_number).zero? }
    end
  end
end
