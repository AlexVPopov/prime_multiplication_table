# frozen_string_literal: true

module CustomPrime
  class Cell
    EMPTY_STRING = ''
    private_constant :EMPTY_STRING

    SPACE = ' '
    private_constant :SPACE

    def initialize(content: EMPTY_STRING, width: 0, padding: SPACE)
      @content = content
      @width = width
      @padding = padding
    end

    def to_s
      content.to_s.rjust(width, padding)
    end

    private

    attr_reader :content, :width, :padding
  end
end
