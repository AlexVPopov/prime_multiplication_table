# frozen_string_literal: true

require 'optparse'
require_relative 'custom_prime/table'

class PrintTable
  DEFAULT_COUNT = 10
  BANNER = "Usage:\n    ruby print_table.rb [options]\n\n"
  SUMMARY = "Displays a multiplication table of prime numbers\n\n"
  OPTIONS = 'Options:'
  COUNT_EXPLANATION = 'The number of primes to be included in the table. Optional. '\
                      'Default: 10'
  INVALID_COUNT_MESSAGE = 'Value for count is invalid. Count must be a positive integer.'
  EXAMPLE = "\nExample:\n    ruby print_table.rb -c 5"

  def initialize
    @options = { count: DEFAULT_COUNT }
    parse_options
  end

  def call
    puts CustomPrime::Table.new(options[:count])
  end

  private

  attr_reader :options

  def parse_options
    ARGV.options do |opts|
      opts.banner = BANNER
      opts.separator SUMMARY
      opts.separator OPTIONS
      opts.on('-c', '--count [INTEGER]', Integer, COUNT_EXPLANATION) do |value|
        if value.nil? || value.zero?
          puts(INVALID_COUNT_MESSAGE)
          exit
        end
        options[:count] = value
      end
      opts.separator EXAMPLE

      opts.parse!
    end
  end
end

PrintTable.new.call
