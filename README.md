# A tool for printing multiplication tables for primes

## Usage

1. `git clone git@gitlab.com:AlexVPopov/prime_multiplication_table.git`
2. `cd prime_multiplication_table`
3. `ruby lib/print_table.rb --help`

## Testing

1. `bundle install`
2. `rspec spec`
